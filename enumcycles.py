def normalize(cycle):
	"""Orders vertices of cycle by increasing order """

	cmin = min(cycle)
	while cycle[0] != cmin:
		v = cycle.pop(0)
		cycle.append(v)

def all_cycles(cycles, path, node, tovisit, adj,K):
	""" Recourcive function enumerating all cycles"""
	for i in adj[node]:
		if i in path:
			j = path.index(i)
			cycle = path[j:]+[node]
			normalize(cycle)
			cycles.add(tuple(cycle))

		if i in tovisit:
			if K-1 > 0:
				all_cycles(cycles,path+[node],i,tovisit-set([i]),adj,K-1)
	return cycles


def get_all_cycles(adj,K):
	""" Function that get all cycles """
	tovisit = set(adj.keys())
	visited = set([])
	cycles = set([])
	for i in tovisit:
		tmpvisit = set(tovisit)
		tmpvisit.remove(i)
		first = i
		all_cycles(cycles,[],first,tmpvisit,adj,K)
	return cycles


