"""
The second block of program which runs optimization problems 
using expectations values computed by exp.py module
"""

import time

from gurobipy import *

import enumcycles
import kep_io


def mk_opt_prob(adj, cycles, E):
    """	Cycle formulation with weight w[c] =E[c]
    """
    model = Model("expectation")
    model.Params.OutputFlag = 0  # silent mode
    x = {}
    cmap, on_cyc = {}, {}
    for i in adj:
        on_cyc[i] = set()
    for (i, cycle) in enumerate(cycles):
        cmap[cycle] = i
        for j in cycle:
            on_cyc[j].add(i)
        x[i] = model.addVar(vtype="B", name="x(%s)" % (i))
    model.update()

    for i in on_cyc:
        cs = on_cyc[i]
        if len(cs) > 1:
            model.addConstr(quicksum(x[j] for j in cs) <= 1, "cycles(%s)" % i)
    model.setObjective(quicksum(E[i] * x[i] for (i, c) in enumerate(cycles)), GRB.MAXIMIZE)
    model.optimize()
    EPS = 0.0001
    sol = [j for j in x if x[j].X > EPS]
    return sorted(sol), model.ObjVal



if __name__ == "__main__":
    import sys

    try:
        K = int(sys.argv[2])
        filename = sys.argv[1]
    except:
        K = 4
        filename = "10_02.input.gz"
    print(filename, "K=", K)

    adj, w, p = kep_io.read_prob(filename)

    st = time.clock()
    cycle = enumcycles.get_all_cycles(adj, K)
    cycle = sorted(cycle, key=lambda c: (len(c), c))
    now = time.clock()
    t_cycles = now - st
    n_cycles = len(cycle)
    print("%d cycles found in %.2f seconds" % (len(cycle), t_cycles))
    sys.stdout.flush()

    # load information on expectations from containers
    E_norec = kep_io.load_containers("pickle_no-recourse_expectations.out")
    E_rec_int = kep_io.load_containers("pickle_int-recourse_expectations.out")

    print("")
    print("****** No-recourse optimization ******")
    z_norec = -1
    t_cycle_norec = 0
    ntr_norec = -1
    if E_norec is not None:
        start = time.clock()
        x_norec, z_norec = mk_opt_prob(adj, cycle, E_norec)
        t_cycle_norec = time.clock() - start
        print("Problem solved in ", t_cycle_norec, " seconds")
        ntr_norec = 0
        for i in x_norec:
            ntr_norec += len(cycle[i])
        print("Objective value for no-recourse", z_norec)
        print("Number of transplants in optimal solution", ntr_norec)
        sys.stdout.flush()
    else:
        print("No-recourse expectations are not available")

    print("")
    print("****** Internal recourse optimization ******")
    z_rec_int = -1
    t_cycle_rec_int = 0
    ntr_rec_int = -1
    if E_rec_int is not None:
        start = time.clock()
        x_rec_int, z_rec_int = mk_opt_prob(adj, cycle, E_rec_int)  # recourse only internally within each cycle or subcycle
        t_cycle_rec_int = time.clock() - start
        print("Problem solved in ", t_cycle_rec_int, " seconds")
        ntr_rec_int = 0
        for i in x_rec_int:
            ntr_rec_int += len(cycle[i])
        print("Objective value for no-recourse", z_rec_int)
        print("Number of transplants in optimal solution", ntr_rec_int)

        sys.stdout.flush()
    else:
        print("Internal-recourse expectations are not available")

    # write results into the output file
    f = open('results_no_int.out', 'a')
    s = str("%.2f\t%.2f\t%d\t%d\t%.2f\t%.2f\n" % (z_norec, z_rec_int, ntr_norec, ntr_rec_int,
                                                  t_cycle_norec, t_cycle_rec_int))
    f.write(s)
    f.close()
