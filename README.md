# KEP_Max_Expectation

The project implements the algorithm for finding maximal expected number of transplants for the Kidney exchange problem (KEP) 
where probabilities of failure of vertices and arc of the compatibility graph are provided.

## Modules
- `kep_io.py` implements routines to read input files
- `enumcycles.py` implements routines to enumerate cycles in the compatibility graph
 
- `exp.py` enumerates all feasible cycles and for each cycle calculates no-recourse and internal-recourse expected number of transplants, saves those expectations into `pickle` containers (files _pickle_no-recourse_expectations.out_ and _pickle_int-recourse_expectations.out_ will be created)
 
- `opt.py` reads information from containers created by `exp.py` and finds a set of vertex disjoint cycles in the graph that has maximal expected number of transplants; it implements Integer Programing (IP) formulation, which is solved using Gurobi Optimization, the module outputs the results to the file _results_no_int.out_
 
- `expss.py` enumerates all feasible subsets and calculates their expected values; subsets are saved into container _pickle_subsets.out_ and their expected values are saved into _pickle_ss_expectations.out_
 
- `optss.py` reads information from containers created by `expss.py` and finds a set of vertex disjoint subsets, implementing IP formulation; the optimal solution is found using Gurobi Optimization; the module outputs the results to the file _results_subsets.out_

## Usage
**_No-recourse and Internal-recourse:_**

    python exp.py <filename> <K>

    python opt.py <filename> <K>

**_Subsets recourse:_**

    python expss.py <filename> <K> <q>
    
    python optss.py <filename> <K> <q>

- `<filename>` - is the name of the file with data; some examples are provided in the project
- `<K>` is the maximum length of cycles
- `<q>` is a parameter for calculating the subsets

**Example of usage**

- for finding no-recourse and internal-recourse maximal expected number of transplants for the graph in _10_02.input.gz_ run:

`python exp.py 10_02.input.gz 3`

`python opt.py 10_02.input.gz 3`

- for finding subset-recourse maximal expected number of transplants for the graph in _10_02.input.gz_ run:

`python expss.py 10_02.input.gz 3 2`

`python optss.py 10_02.input.gz 3 2`


