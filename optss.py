"""
The second block of program which runs optimization problem 
for the subsets recourse scheme using expectations values computed by expss.py module
"""

from gurobipy import *

import exp


def mk_opt_prob_ss(adj, subsets, E):
    """	Optimization model to be solved to find
        an optimal solution for the problem maximizing the expected
        number of transplants within subsets recourse scheme
    """
    model = Model("SSexpectation")
    model.Params.OutputFlag = 0  # silent mode
    x = {}
    cmap, on_ss = {}, {}
    for i in adj:
        on_ss[i] = set()
    for i in subsets:
        ss = subsets[i]
        cmap[ss] = i
        for j in ss:
            on_ss[j].add(i)
        x[i] = model.addVar(vtype="B", name="x(%s)" % (i))
    model.update()
    for i in on_ss:
        cs = on_ss[i]
        if len(cs) > 1:
            model.addConstr(quicksum(x[j] for j in cs) <= 1, "cyc(%s)" % i)
    model.setObjective(quicksum(E[i] * x[i] for i in subsets), GRB.MAXIMIZE)
    model.optimize()
    EPS = 0.0001
    sol = [j for j in x if x[j].X > EPS]
    return sorted(sol), model.ObjVal


if __name__ == "__main__":
    import sys

    try:
        K = int(sys.argv[2])
        q = int(sys.argv[3])
        filename = sys.argv[1]
    except:
        K = 3
        q = 2
        filename = "10_02.input.gz"
    print("==================================================")
    print(filename, "K=", K)

    import kep_io

    adj, w, p = kep_io.read_prob(filename)

    # enumerate cycles
    import time
    import enumcycles

    st = time.clock()
    cycle = enumcycles.get_all_cycles(adj, K)
    cycle = sorted(cycle, key=lambda c: (len(c), c))
    now = time.clock()
    t_cycles = now - st
    n_cycles = len(cycle)
    print(len(cycle), "cycles were found in", t_cycles, " seconds")
    sys.stdout.flush()

    # load subsets from containers
    subsets = kep_io.load_containers("pickle_subsets.out")
    E_rec_subsets = kep_io.load_containers("pickle_ss_expectations.out")

    print("")
    print("****** Subsets recourse optimization ******")

    z_rec_ss = -1
    t_cycle_rec_subsets = 0
    ntr_rec_ss = -1
    if E_rec_subsets is None:
        print("Subset-recourse expectations are not available")
    else:

        start = time.clock()
        x_rec_ss, z_rec_ss = mk_opt_prob_ss(adj, subsets, E_rec_subsets)
        now = time.clock()
        t_cycle_rec_subsets = now - start
        print("Problem solved in %2.f seconds" % t_cycle_rec_subsets)

        ntr_rec_ss = 0
        for i in x_rec_ss:
            ss = set(subsets[i])
            cind = [j for (j, s) in enumerate(cycle) if ss.issuperset(s)]
            nn = exp.solve_kep_enum(cycle, cind)
            ntr_rec_ss += nn

        print("Optimal value ", z_rec_ss)
        print("Number of transplants in optimal solution", ntr_rec_ss)
        sys.stdout.flush()

    # write results into the output file
    f = open('results_subsets.out', 'a')
    s = str("%.2f\t%d\t%.2f\n" % (z_rec_ss, ntr_rec_ss, t_cycle_rec_subsets))
    f.write(s)
    f.close()
