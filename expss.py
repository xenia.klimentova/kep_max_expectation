"""
	The first block of the code for subsets recourse
	Enumrates all subsets and computes subset-recourse expectations for them
	May be run by pypy
"""

import kep_io
import time
import enumcycles
import exp

SS_ = set()


def enumerate_subsets(cycle, V, q):
    """ Enumerate all the relevant subsets.
        prepares an input and runs enum_subsets recursive funtion
    """

    for c in cycle:
        rv = set(V)
        rv -= set(c)
        setc = set(c)
        if enum_subsets(cycle, setc, setc, rv, q) < 0:
            return -1
    return 0


def enum_subsets(cycle, ss0, ss, rv, q):
    """ Recursive function to find all relevant subsets"""
    global SS_
    global Tss  # Starting time of enumeration of the subsets
    global MAXTss  # Limit of time to run the function

    # check if the time limitis not acieved yet
    rv_ = set(rv)
    if (time.clock() - Tss) > MAXTss:
        return -1

    while len(rv_) > 0:
        k = rv_.pop()
        ss_ = set(ss)
        ss_.add(k)
        nocycle = True
        for c in cycle:
            setc = set(c)
            if setc.issubset(ss_) and k in c and ss0 & setc:
                SS_.add(frozenset(ss_))
                nocycle = False
                break
        if nocycle:
            continue

        if len(ss_) == K + q:
            continue
        rv1 = set(rv)
        rv1 -= set(ss_)
        if enum_subsets(cycle, ss0, ss_, rv1, q) < 0:
            return -1
        if len(rv_) == 0:
            continue
    return 0


def rec_subs(cycle, p, SS, stopt):
    """	Computes expected values for subsets SS
        Prerares input and runs recursive procedure from exp.py
    """
    E = {}
    for i in SS:
        if time.clock() > stopt:
            return None
        ss = set(SS[i])
        cind = [j for (j, s) in enumerate(cycle) if ss.issuperset(s)]
        rA = set()
        for j in cind:
            c = cycle[j]
            rA |= set((c[k - 1], c[k]) for k in range(len(c)))
        E[i] = exp.rec(cycle, ss, rA, cind, p, stopt, bp=1)

    return E


if __name__ == "__main__":

    import sys

    try:
        K = int(sys.argv[2])
        q = int(sys.argv[3])
        filename = sys.argv[1]
    except:
        K = 3
        q = 2
        filename = "10_02.input.gz"
    print("==================================================")
    print(filename, "K=", K)

    # read data from file

    adj, w, p = kep_io.read_prob(filename)

    st_cycles = time.clock()
    cycle = enumcycles.get_all_cycles(adj, K)
    cycle = sorted(cycle, key=lambda c: (len(c), c))
    t_cycles = time.clock() - st_cycles
    n_cycles = len(cycle)
    print("%d cycles were found in %.2f seconds "%(len(cycle), t_cycles))
    sys.stdout.flush()

    st_subsets = time.clock()

    # enumerating of subsets

    # Add all cycles
    V = set()
    for c in cycle:
        V |= set(c)
        SS_.add(frozenset(c))

    # run function for enumeration of subsets
    MAXTss = 1800  # Time limit to enumerate subsets
    Tss = time.clock()  # start time of enumeration of subsets
    ret = enumerate_subsets(cycle, V, q)
    t_subsets = time.clock() - st_subsets

    n_subsets = -1
    E_rec_subsets = None
    subsets = None
    t_rec_ss = -1
    if ret == 0:
        # if time limit was not exceeded
        n_subsets = len(SS_)

        print("%d subsets were found in %.2f seconds" % (len(SS_), t_subsets))
        sys.stdout.flush()

        subsets = {}
        for (i, ss) in enumerate(SS_):
            subsets[i] = ss

        # calculate expectation values for all subsets
        start = time.clock()
        T_ = time.clock()  # start time for calculation of subsets
        MAXT = 3600  # Time limit for calculation of expectations of subsets
        stopt = T_ + MAXT
        E_rec_subsets = rec_subs(cycle, p, subsets, stopt)  # recourse only internally within each cycle or subcycle
        if E_rec_subsets is None:
            print("Time limit is achieved when subset-recourse expectations are calculated. Exiting.")
        now = time.clock()
        t_rec_ss = now - start
        print("Expectations calculated in %.2f seconds" % t_rec_ss)
        sys.stdout.flush()
    else:
        print("Enumerate subsets: time limit achieved. Exiting")

    # save information on subsets and their expectations into the containers
    kep_io.save_containers(subsets, "pickle_subsets.out")
    kep_io.save_containers(E_rec_subsets, "pickle_ss_expectations.out")

    # write to output file
    f = open('results_subsets.out', 'a')
    s = str("%s\t%d\t%d\t%d\t%.2f\t%d\t%.2f\t%.2f\t" % (filename, K, q, n_cycles, t_cycles, n_subsets,
                                                        t_subsets,  t_rec_ss))
    f.write(s)
    f.close()
