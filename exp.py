import operator
import time
from functools import reduce

import enumcycles
import kep_io

"""
The first block of code which enumerate all cycles and calculate expected values for them
Should be followed by opt.py
As do not have any call of Gurobi optimizer, may be run by pypy
"""


def relevant_arcs(adj, c):
    """ Enumerates all arcs of the cycle c and return them"""
    A = set()
    for i in c:
        for j in adj[i]:
            if j in c:
                A.add((i, j))
    return A


def prod(factors):
    """ Calculates a prodact of several factors"""
    return reduce(operator.mul, factors, 1)


t_gurobi = 0


def maxdv(setV, cycles):
    """Finds a vertex in setV which is involved in a maximum number of cycles"""
    V = list(setV)
    deg = [0 for i in V]
    for i in range(len(V)):
        for c in cycles:
            if V[i] in c:
                deg[i] += 1
    u = deg.index(max(deg))
    return V[u]


def maxda(setA, cycles):
    """Finds a ertex in setA which is involved in a maximum number of cycles"""
    A = list(setA)
    deg = [0 for i in A]
    for i in range(len(A)):
        deg.append(0)
    for c in cycles:
        for i in range(len(c)):
            if (c[i - 1], c[i]) in A:
                deg[A.index((c[i - 1], c[i]))] += 1
    u = deg.index(max(deg))
    return A[u]


def solve_kep_enum(cycle, cind):
    """ Solve the cycle packing problem in a leaf of the search tree by enumeration"""
    import time
    st = time.clock()
    rc_tocheck = set(cind)
    rc_tocheck = sorted(rc_tocheck, key=lambda i: (len(cycle[i]), i))
    packing = set()
    z = enum_cyc(cycle, -1, packing, rc_tocheck)
    return z


def enum_cyc(cycle, z, packing, rc):
    """ Recursive procedure for finding a solution in a leaf by enumeration"""
    while len(rc) > 0:
        k = rc.pop()
        rc_ = set(rc)
        for j in rc:
            if set(cycle[k]).intersection(set(cycle[j])):
                rc_.remove(j)
        if len(rc_) == 0:
            lenp = 0
            for t in packing:
                lenp += len(cycle[t])
            lenp += len(cycle[k])
            if lenp > z:
                z = lenp
            continue

        packing_ = set(packing)
        packing_.add(k)
        z = enum_cyc(cycle, z, packing_, rc_)
    return z


def no_rec(cycle, p):
    """no_rec: Calclates expectations for cycles in cycle if there is no recourse"""
    E = {}
    for i, c in enumerate(cycle):
        # c = cycle[i]
        F = 1
        n = len(c)
        for j in range(n):
            v = c[j]
            arc = c[j - 1], c[j]
            F *= (1 - p[v])
            F *= (1 - p[arc])
        E[i] = n * F
    return E


def rec(cycle, remv, rema, cind, p, stopt, bp):
    """ Recurcive procedure to find the internal recource expectation of the cycle
		Nodes for the search tree:
			remv - set of vertice of a given cycle
			reva - set of arcs of a given cycle
		cind - set of indeces of enclosed cycles
			
		Other input:
			cycle - set of all cycles
			p - given probabilities
			bp - current expectation  value (1 in input) (is s in description of the algorithm)
	"""

    # Check whether time limis was exceeded
    if (time.clock() > stopt):
        return None

    # Fathoming in case of empty set of enclosed cycles current expectation is equal to 0
    if cind == [] or bp == 0:
        return 0
    # Fathoming in case there is only one enclosed cycle. Add the corresponding value to the expectation expression
    elif len(cind) == 1:
        c = cycle[cind[0]]
        E = bp * len(c) * prod((1 - p[i]) for i in c if i in remv) * prod(
            1 - p[c[i - 1], c[i]] for i in range(len(c)) if (c[i - 1], c[i]) in rema)
        return E
    # Fathoming in case the leaf is achieved
    elif len(remv) == 0 and len(rema) == 0:
        """ Check if there exists a cycle in cind 
			such that its number of vertices is equal 
			to the number of vertices in all remaining cycles cind
			if yes - it is a solution
		"""
        vv = set()  # count number of vertices in remaining cycles
        for i in cind:
            vv |= set(cycle[i])
        lngth = False
        for i in cind:
            if (len(cycle[i]) == len(vv)):
                z = len(vv)
                lngth = True
                break
        if lngth:
            return z * bp
        # solve the cycle packing problem in a leaf for the set of cycle cind
        z = solve_kep_enum(cycle, cind)
        E = bp * z
        return E

    # Branching"
    (V, A) = (remv, rema)
    rcycles = [cycle[i] for i in cind]
    # if set of nodes for branching contains a vertex, we branch on vertex first
    if (len(V) > 0):
        # choose the vertex that is involved in the maximum number of enclosed cycles
        u = maxdv(V, rcycles)
        V0 = set(V)
        V0.remove(u)
        A0 = set(A)
        V1 = set(V0)
        A1 = [(i, j) for (i, j) in A if i != u and j != u]
        p0 = bp * (1 - p[u])
        p1 = bp * p[u]
        rc1 = [i for i in cind if u not in cycle[i]]
    # if there is no vertices in the set of node, branch on the first arc in the list
    elif (len(A) > 0):
        V0 = set(V)
        V1 = set(V)
        A0 = set(A)
        (u, v) = A.pop()
        A0.remove((u, v))
        A1 = set(A0)
        p0 = bp * (1 - p[u, v])
        p1 = bp * p[u, v]
        rc1 = []
        for j in cind:
            c = cycle[j]
            incycle = False
            for i in range(len(c)):
                if (c[i - 1] == u and c[i] == v):
                    incycle = True
            if not incycle:
                rc1.append(j)

    # recursive call of the function fot the left branch
    E0 = rec(cycle, V0, A0, list(cind), p, stopt, p0)
    # recursive call of the function fot the right branch
    E1 = rec(cycle, V1, A1, rc1, p, stopt, p1)
    if E0 is None or E1 is None:
        return None
    else:
        return E0 + E1


E_ = {}  # used for cache of calculated expectations


def rec_int(cycle, p, stopt):
    """Computes internal recourse expectations for all cycles"""
    import time

    E = {}
    for (i, c) in enumerate(cycle):

        rV = set(c)
        rA = relevant_arcs(adj, c)
        S = set(c)

        cind = [j for (j, s) in enumerate(cycle) if S.issuperset(s)]
        E[i] = rec(cycle, rV, rA, cind, p, stopt, bp=1)
        if time.clock() > stopt:
            print("Time limit is achieved when internal-recourse expectations are calculated. Exiting.")
            return None

    return E


if __name__ == "__main__":
    import sys

    try:
        K = int(sys.argv[2])
        filename = sys.argv[1]
    except:
        K = 4
        filename = "10_02.input.gz"
    print("==================================================")
    print(filename, "K=", K)

    # read data from file
    adj, w, p = kep_io.read_prob(filename)

    # find cycles
    st = time.clock()
    cycle = enumcycles.get_all_cycles(adj, K)
    cycle = sorted(cycle, key=lambda c: (len(c), c))
    t_cycles = time.clock()- st
    n_cycles = len(cycle)
    print("%d cycles found in %.2f seconds" % (len(cycle), t_cycles))
    sys.stdout.flush()

    # ===============================================

    # calculate no-recourse expectations
    start = time.clock()
    E_norec = no_rec(cycle, p)
    t_norec = time.clock() - start
    print("No-recourse expectations are calculated in %.2f seconds" % t_norec)
    sys.stdout.flush()

    # calculate internal-recourse expectations
    start = time.clock()
    T_ = time.clock()
    MAXT = 1800
    stopt = T_ + MAXT
    E_rec_int = rec_int(cycle, p, stopt)
    t_rec_int = time.clock() - start
    print("Internal recourse expectations are calculated in %.2f seconds" % t_rec_int)
    sys.stdout.flush()

    # write information on expectations into containers
    kep_io.save_containers(E_norec, "pickle_no-recourse_expectations.out")
    kep_io.save_containers(E_rec_int, "pickle_int-recourse_expectations.out")

    # write to output file
    f = open('results_no_int.out', 'a')
    s = str("%s\t%d\t%d\t%.2f\t%.2f\t%.2f\t" % (filename, K, n_cycles, t_cycles, t_norec, t_rec_int))
    f.write(s)
    f.close()
